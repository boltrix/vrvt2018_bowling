﻿using UnityEngine;
using System.Collections;

public class Drag_ball : MonoBehaviour
{

    [SerializeField] private Rigidbody BowlingBall; //prefab
    [SerializeField] private Transform throwOrigin;

    private int count = 0;
    public float timeInSeconds = 0;
    bool canThrow = true;

    public void Catchmousewheel()
    {
        float scroll = Input.GetAxis("Mouse ScrollWheel"); //only forward
            if (scroll > float.Epsilon)
                {
                    count++;
                    Debug.Log("Current scroll: " + count);
                }
    }


    private void CalcTime()
    {
        
        float force = 0;
            if (count >= 2)
            {
                timeInSeconds += Time.deltaTime;
                if (count == 4 && canThrow == true)
                {
                    Debug.Log("Catched count == 4");
                    count = 0;                 
                    force = 15 - timeInSeconds;
                    Vector3 impulseForce = new Vector3(0.0f, 0.0f, -force);
                    GameObject spawnedPrefab = Instantiate(BowlingBall.gameObject, throwOrigin.position, throwOrigin.rotation);
                    spawnedPrefab.GetComponent<Rigidbody>().AddForce(transform.forward* force, ForceMode.Impulse);
                    spawnedPrefab.GetComponent<Rigidbody>().useGravity = true;
                    GameObject DB = GameObject.Find("BowlingBall1");
                    Destroy(DB);
                Debug.Log("Destroy");
                Destroy(spawnedPrefab, 10f);
                    canThrow = false;
                    Debug.Log("ApplyForce: " + force);              
                    timeInSeconds = 0;              
                }
            
            }
    }

 
    
    void Update()
    {
        
        Catchmousewheel();
        CalcTime();
  
    }
}



