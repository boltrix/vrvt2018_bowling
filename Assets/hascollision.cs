﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hascollision : MonoBehaviour
{

    IEnumerator coroutineA()
    {
        // wait for 1 second
        Debug.Log("coroutineA created");
        yield return new WaitForSeconds(1.0f);
        Renderer rend = GetComponent<Renderer>();
        float result = Mathf.Lerp(3f, 5f, 0.5f);
        rend.material.shader = Shader.Find("_Color");
        Color newColor = new Color(result, result * 2,5, result * 3);
        rend.material.SetColor("_Color", newColor);
        

    }


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Pin" || collision.gameObject.name == "Pin (1)" || collision.gameObject.name == "Pin (2)" || collision.gameObject.name == "Pin (4)" || collision.gameObject.name == "Pin (5)" || collision.gameObject.name == "Pin (8)" || collision.gameObject.name == "Pin (9)")
        {
            StartCoroutine(coroutineA());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
